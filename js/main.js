$(document).ready(function () {
    $('.slider').slick({
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        speed: 1500,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
    });

    $(document).on('click', '.limiter-item', function () {
        $(this).addClass('selected').siblings().removeClass('selected');
    });


    //Pagination

    pageSize = 16;

    var pageCount =  $(".product_container-product").length / pageSize;

    for(var i = 0 ; i<pageCount;i++){

        $("#pagin").append('<li><a href="#">'+(i+1)+'</a></li> ');
    }
    $("#pagin li").first().find("a").addClass("current")
    showPage = function(page) {
        $(".product_container-product").hide();
        $(".product_container-product").each(function(n) {
            if (n >= pageSize * (page - 1) && n < pageSize * page)
                $(this).show();
        });
    }

    showPage(1);

    $("#pagin li a").click(function() {
        $("#pagin li a").removeClass("current");
        $(this).addClass("current");
        showPage(parseInt($(this).text()))
    });

    // Slider gallery product


    $('.product-gallery_main_image').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        asNavFor: '.product-gallery_nav'
    });
    $('.product-gallery_nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.product-gallery_main_image',
        dots: false,
        focusOnSelect: true,
        prevArrow: $('.bottom_text-slider_prev'),
        nextArrow: $('.bottom_text-slider_next')
    });

    // Slider bottom_text

    $('.bottom_text-slider_container').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: $('.bottom_text-slider_prev'),
        nextArrow: $('.bottom_text-slider_next')
    });
});